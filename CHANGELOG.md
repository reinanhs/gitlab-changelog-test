## Changelo
## 1.0.5 (2022-04-11)

### Changed (1 change)

- [Atualiza o arquivo de readme](reinanhs/gitlab-changelog-test@0956833175d30f60636e4bf54418f8b245711891)

## 1.0.4 (2022-04-11)

No changes.

## 1.0.3 (2022-04-11)

### Changed (1 change)

- [Update readme](reinanhs/gitlab-changelog-test@d7ee7d760a2130dcc2ca407c58e154fd7062f393)

## 1.0.2 (2022-04-11)

### Added (1 change)

- [Added new info in readme](reinanhs/gitlab-changelog-test@dea687223fa678a7482950f08bc0dbee3aca1e18)

### Security (1 change)

- [Security update info](reinanhs/gitlab-changelog-test@49bb59f52946a60e0efc487f3f37f481933708c2)

## 1.0.1 (2022-04-11)

### Changed (1 change)

- [Update readme](reinanhs/gitlab-changelog-test@db3b1f091d6bc6f6f509f6a5125077319e173325)

## 1.0.0 (2022-04-11)

No changes.
