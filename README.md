# gitlab-changelog-test

Version 1.0

## Text

Now that we are using gitaly to compile git, the git version isn't known
from the manifest, instead we are getting the gitaly version. Update our
vendor field to be `gitlab` to avoid cve matching old versions.

## Added

x = b2 + c4
y = 42

## Security

For customization, a configuration file in YAML format is used. The file must be located in the root of the repository along the path: `.gitlab/changelog_config.yml`

```
curl --header "PRIVATE-TOKEN: token" --data "version=1.0.2&branch=main" "https://gitlab.com/api/v4/projects/35260635/repository/changelog"
```

## Documentation 

- [Automatic generation of a project change log using GitLab](https://prog.world/automatic-generation-of-a-project-change-log-using-gitlab/)
- [Changelog entries](https://docs.gitlab.com/ee/development/changelog.html)
- [Generate changelog data](https://docs.gitlab.com/ee/api/repositories.html#generate-changelog-data)
